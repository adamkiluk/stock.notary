﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using STOCK.NOTARY.Application.Common.Interfaces;

namespace STOCK.NOTARY.Persistence
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddPersistence(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<StockNotaryDbContext>(options =>
                options.UseSqlServer(configuration.GetConnectionString("StockNotaryDatabase")));

            services.AddScoped<IStockNotaryDbContext>(provider => provider.GetService<StockNotaryDbContext>());

            return services;
        }
    }
}
