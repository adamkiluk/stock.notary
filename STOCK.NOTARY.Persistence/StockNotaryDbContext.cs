﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using STOCK.NOTARY.Application.Common.Interfaces;
using STOCK.NOTARY.Common;
using STOCK.NOTARY.Domain.Common;
using System.Threading;
using System.Threading.Tasks;

namespace STOCK.NOTARY.Persistence
{
    public class StockNotaryDbContext : DbContext, IStockNotaryDbContext
    {
        private readonly ICurrentUserService _currentUserService;
        private readonly IDateTime _dateTime;

        public StockNotaryDbContext(DbContextOptions<StockNotaryDbContext> options)
            : base(options)
        {
        }

        public StockNotaryDbContext(
            DbContextOptions<StockNotaryDbContext> options,
            ICurrentUserService currentUserService,
            IDateTime dateTime)
            : base(options)
        {
            _currentUserService = currentUserService;
            _dateTime = dateTime;
        }

        //Dbsets here

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            foreach (var entry in ChangeTracker.Entries<AuditableEntity>())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.Entity.CreatedBy = _currentUserService.UserId;
                        entry.Entity.Created = _dateTime.Now;
                        break;
                    case EntityState.Modified:
                        entry.Entity.LastModifiedBy = _currentUserService.UserId;
                        entry.Entity.LastModified = _dateTime.Now;
                        break;
                }
            }

            return base.SaveChangesAsync(cancellationToken);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(StockNotaryDbContext).Assembly);
        }
    }
}
