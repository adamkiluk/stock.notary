﻿using Microsoft.EntityFrameworkCore;

namespace STOCK.NOTARY.Persistence
{
    public class StockNotaryContextFactory : DesignTimeDbContextFactoryBase<StockNotaryDbContext>
    {
        protected override StockNotaryDbContext CreateNewInstance(DbContextOptions<StockNotaryDbContext> options)
        {
            return new StockNotaryDbContext(options);
        }
    }
}
