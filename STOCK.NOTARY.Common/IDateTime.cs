﻿using System;

namespace STOCK.NOTARY.Common
{
    public interface IDateTime
    {
        DateTime Now { get; }
    }
}
