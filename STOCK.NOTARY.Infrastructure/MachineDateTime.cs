﻿using STOCK.NOTARY.Common;
using System;

namespace STOCK.NOTARY.Infrastructure
{
    public class MachineDateTime : IDateTime
    {
        public DateTime Now => DateTime.Now;

        public int CurrentYear => DateTime.Now.Year;
    }
}
