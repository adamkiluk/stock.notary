﻿using Microsoft.AspNetCore.Identity;

namespace STOCK.NOTARY.Infrastructure.Identity
{
    public class ApplicationUser : IdentityUser
    {
    }
}
