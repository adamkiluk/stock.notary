﻿using STOCK.NOTARY.Application.Common.Interfaces;
using STOCK.NOTARY.Application.Notifications.Models;
using System.Threading.Tasks;

namespace STOCK.NOTARY.Infrastructure
{
    public class NotificationService : INotificationService
    {
        public Task SendAsync(MessageDto message)
        {
            return Task.CompletedTask;
        }
    }
}
