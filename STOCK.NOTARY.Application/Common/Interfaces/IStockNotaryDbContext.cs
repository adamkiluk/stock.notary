﻿using System.Threading;
using System.Threading.Tasks;

namespace STOCK.NOTARY.Application.Common.Interfaces
{
    public interface IStockNotaryDbContext
    {
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
