﻿using STOCK.NOTARY.Application.Notifications.Models;
using System.Threading.Tasks;

namespace STOCK.NOTARY.Application.Common.Interfaces
{
    public interface INotificationService
    {
        Task SendAsync(MessageDto message);
    }
}
